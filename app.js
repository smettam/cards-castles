/** app.js **/

var express = require('express');
var app = express();
var server = require('http').Server(app);
var db = require('./server/js/db/DB');

// Start Server
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/client/index.html');
});
app.use('/client', express.static(__dirname + '/client'));

server.listen(80);

// Import Game
Game = require('./server/js/model/Game');

// Retrieve card data from database
db.findCards().then(function (items) {
    items.forEach(function (data) {
        CardData.uniqueCards.push(new CardData(data));
    });
    console.log('Successfully connected to database.');
    startApp();
}, function (err) {
    console.error('Failed to connect to database.', err, err.stack);
});

// Start the application
function startApp() {
    console.log('\nApplication Started');

    var connections = [];
    var matchmakingPool = [];
    var runningGames = [];

    // SET UP SOCKET EVENTS
    var io = require('socket.io')(server);

    // CONNECT
    io.on('connection', function (socket) {
        connections.push(socket);

        console.log("\nPlayer Connected");
        console.log("Total Connections: " + connections.length);

        // DISCONNECT
        socket.on('disconnect', function () {
            // If the player that disconnected was in a game, notify
            // the other player that their opponent disconnected and
            // remove the game from the runningGames list.
            var gameToRemove;
            runningGames.forEach(function (game) {
                game.players.forEach(function (player) {
                    if (player.socket == socket) {
                        var otherPlayer = game.players[1 - game.players.indexOf(player)];
                        otherPlayer.socket.emit('enemyDisconnected');
                        gameToRemove = game;
                    }
                });
            });
            if (typeof gameToRemove != 'undefined') {
                runningGames.splice(runningGames.indexOf(gameToRemove), 1);
                console.log("\nA Game has Ended");
                console.log("Total Running Games: " + runningGames.length)
            }

            // If the player was in the matchmaking queue, remove them.
            var clientToRemove;
            matchmakingPool.forEach(function (client) {
                if (client == socket) {
                    clientToRemove = client;
                }
            });
            if (typeof clientToRemove != 'undefined') {
                matchmakingPool.splice(matchmakingPool.indexOf(clientToRemove), 1);
                console.log("\nPlayer Left Matchmaking Queue");
                console.log("Matchmaking Pool Size: " + matchmakingPool.length);
            }

            // Finally, remove the player from the connections list.
            connections.splice(connections.indexOf(socket), 1);


            console.log("\nPlayer Disconnected");
            console.log("Total Connections: " + connections.length);
        });

        // PLAY (FIND MATCH)
        socket.on('play', function () {
            matchmakingPool.push(socket);

            console.log("\nPlayer Joined Matchmaking Queue");
            console.log("Matchmaking Pool Size: " + matchmakingPool.length);

            if (matchmakingPool.length == 2) {
                matchmakingPool[0].emit('match found');
                matchmakingPool[1].emit('match found');

                var newGame = new Game(matchmakingPool[0], matchmakingPool[1]);
                runningGames.push(newGame);

                console.log("\nNew Game Started");
                console.log("Total Running Games: " + runningGames.length);

                matchmakingPool = [];
            }
        });

        // CANCEL FIND MATCH
        socket.on('cancel', function () {
            matchmakingPool.splice(matchmakingPool.indexOf(socket), 1);
            console.log("\nPlayer Left Matchmaking Queue");
            console.log("Matchmaking Pool Size: " + matchmakingPool.length);
        });

        // READY TO PLAY
        socket.on('readyToPlay', function () {
            runningGames.forEach(function (game) {
                game.players.forEach(function (player) {
                    if (player.socket == socket) {
                        game.noOfPlayersReady++;
                        if (game.noOfPlayersReady == 2) {
                            game.gameController.startMatch();
                        }
                    }
                });
            });
        });

        // END TURN
        socket.on('endTurn', function () {
            runningGames.forEach(function (game) {
                game.players.forEach(function (player) {
                    if (player.socket == socket) {
                        if (game.gameController.currentPlayer == player) {
                            game.gameController.endTurn();
                        }
                    }
                });
            });
        });

        // PLAY CARD
        socket.on('playCard', function (data) {
            runningGames.forEach(function (game) {
                game.players.forEach(function (player) {
                    if (player.socket == socket) {
                        if (game.gameController.currentPlayer == player) {
                            game.gameController.playCardAt(data.cardIndex);
                        } else {
                            player.socket.emit('notYourTurn');
                        }
                    }
                });
            });
        });

        // ATTACK
        socket.on('attack', function (data) {
            runningGames.forEach(function (game) {
                game.players.forEach(function (player) {
                    if (player.socket == socket) {
                        if (game.gameController.currentPlayer == player) {
                            game.gameController.issueAttack(data.attackingCardIndex, data.isTargetCastle, data.targetIndex);
                        } else {
                            player.socket.emit('notYourTurn');
                        }
                    }
                });
            });
        });
    });

    // Periodically check to see if any games have ended and remove
    // them from the list of running games.
    setInterval(function () {
        var gameToRemove;
        runningGames.forEach(function (game) {
            if (game.gameController.gameOver) {
                gameToRemove = game;
            }
        });

        if (typeof gameToRemove != 'undefined') {
            runningGames.splice(runningGames.indexOf(gameToRemove), 1);
            console.log("\nA Game has Ended");
            console.log("Total Running Games: " + runningGames.length)
        }
    }, 100);
}