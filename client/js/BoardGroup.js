/** BoardGroup.js **/

/* This class extends Phaser.Group */

var BoardGroup = function (game, x, y) {
    Phaser.Group.call(this, game);

    this.boardPosition = {
        x: x,
        y: y
    };
};

BoardGroup.prototype = Object.create(Phaser.Group.prototype);
BoardGroup.prototype.constructor = BoardGroup;

/* Override */
BoardGroup.prototype.add = function (card) {
    Phaser.Group.prototype.add.call(this, card);

    this.repositionCards();
};

/* Override */
BoardGroup.prototype.remove = function (child) {
    Phaser.Group.prototype.remove.call(this, child);

    this.repositionCards();
};

BoardGroup.prototype.repositionCards = function () {
    this.boardAreaWidth = 1100;

    for (var i = 0; i < this.length; i++) {

        var card = this.getChildAt(i);
        var boardArea = {
            left: game.world.centerX - this.boardAreaWidth / 2,
            right: game.world.centerX + this.boardAreaWidth / 2
        }
        var newCardPos = {
            x: boardArea.left + (1 / (this.length + 1)) * (boardArea.right - boardArea.left) * (i + 1),
            y: this.boardPosition.y
        };

        var tween = game.add.tween(card).to({
            x: newCardPos.x,
            y: newCardPos.y
        }, 500, Phaser.Easing.Quadratic.Out);
        tween.start();
    }
};