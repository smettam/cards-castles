var bootState = {

    create: function () {
        
        game.scale.pageAlignHorizontally = true;
        game.scale.pageAlignVertically = true;

        // Allows the game to keep running updates even when the browser windowloses focus.
        game.stage.disableVisibilityChange = true;        
        
        game.state.start('load');
    }
}