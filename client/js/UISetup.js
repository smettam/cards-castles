/** UISetup.js **/

var UISetup = function (game, playState) {

    this.largeFont = {
        font: '80px Almendra',
        fill: '#fff',
        stroke: '#000',
        strokeThickness: 6
    };

    this.smallFont = {
        font: '40px Almendra',
        fill: '#fff',
        stroke: '#000',
        strokeThickness: 4
    };
};

UISetup.prototype.initialiseUI = function () {
    //Must draw background before everything else.
    game.add.sprite(0, 0, 'background');
    this.initialiseEndTurnButton();
    this.initialiseEnemyTurnLabel();
    this.initialiseCastleViews();
    this.initialiseDeckView();
    this.initialiseBoard();
    this.initialiseHand();
    this.initialiseInfoLabels();
    this.initialiseManaLabel();
    this.initialiseMenu();
};

// Button that appears on the players turn and can be clicked on end their turn.
UISetup.prototype.initialiseEndTurnButton = function () {
    playState.endTurnButton = game.add.button(game.world.width - 150, game.world.centerY, 'endturn', playState.endTurn, playState, 2, 0, 1);
    playState.endTurnButton.anchor.setTo(0.5, 0.5);
    playState.endTurnButton.visible = false;
    playState.endTurnButton.smoothed = false;
};

// Label that appears when the enemy is taking their turn.
UISetup.prototype.initialiseEnemyTurnLabel = function () {
    playState.enemyTurnLabel = game.add.sprite(game.world.width - 175, game.world.centerY, 'enemy-turn');
    playState.enemyTurnLabel.anchor.setTo(0.5);
    playState.enemyTurnLabel.visible = true;
    playState.enemyTurnLabel.smoothed = false;
};

// Graphics and labels Castles.
UISetup.prototype.initialiseCastleViews = function () {
    // Player's Castle Sprite and Health Label
    playState.castle = new CastleView(game, game.world.centerX, game.world.height - 175, 'castle', this.largeFont);

    // Enemy Castle Sprite and Health Label
    playState.enemyCastle = new CastleView(game, game.world.centerX, 90, 'castle', this.largeFont);
};

// Graphic that represents the players deck. When cards are drawn they 
// initially spawn at the location of the deck.
UISetup.prototype.initialiseDeckView = function () {
    playState.deck = game.add.sprite(game.world.width - 150, game.world.centerY + 200, 'card-back');
    playState.deck.anchor.setTo(0.5);
    playState.deck.smoothed = false;
};

UISetup.prototype.initialiseHand = function () {
    // Group for positioning all the cards in the player's hand.
    playState.hand = new HandGroup(game, game.world.centerX, game.world.height + 25);
};

UISetup.prototype.initialiseBoard = function () {
    // Group for positioning all the cards on the player's board.
    playState.board = new BoardGroup(game, game.world.centerX, game.world.centerY + 75);

    // Group for positioning all the cards on the enemy player's board
    playState.enemyBoard = new BoardGroup(game, game.world.centerX, game.world.centerY - 150);
};

// Labels used to alert the player when it is their turn or other
// important information.
UISetup.prototype.initialiseInfoLabels = function () {

    playState.infoLabel = game.add.text(game.world.centerX, game.world.centerY, 'Your Turn', this.largeFont);
    playState.infoLabel.anchor.setTo(0.5);
    playState.infoLabel.alpha = 0;
    playState.infoLabelTimeout = 'undefined';

    playState.smallInfoLabel = game.add.text(game.world.centerX, game.world.centerY + 100, '', this.smallFont);
    playState.smallInfoLabel.anchor.setTo(0.5);
    playState.smallInfoLabel.alpha = 0;
    playState.smallInfoLabelTimeout = 'undefined';
};

// Label that displays the players current mana and total mana pool. 
UISetup.prototype.initialiseManaLabel = function () {
    playState.manaLabel = game.add.text(250, game.world.height - 40, 'Mana: 0/0', this.smallFont);
    playState.manaLabel.anchor.setTo(0.5);
};

UISetup.prototype.initialiseMenu = function () {
    playState.menuLabel = game.add.text(game.world.width - 10, 10, 'Menu', this.smallFont);
    playState.menuLabel.anchor.x = 1;

    playState.menu = game.add.sprite(game.world.centerX, game.world.centerY, 'menu');
    playState.menu.anchor.setTo(0.5);
    playState.menu.visible = false;

    playState.menuLabel.inputEnabled = true;
    playState.menuLabel.input.useHandCursor = true;
    playState.menuLabel.events.onInputDown.add(function () {

        playState.menu.visible = !playState.menu.visible;
    });
};