var playState = {

    pickedUpCard: 'undefined',
    selectedCard: 'undefined',
    target: 'undefined',

    create: function () {
        // Fade the game into view from black.
        this.camera.flash('#000', 1000);

        // Notify the server when the playState has started.
        this.camera.onFlashComplete.addOnce(function () {
            socket.emit('readyToPlay');
        });
        
        //Must draw background before everything else.
        game.add.sprite(0, 0, 'background');
        
        this.impactSound = game.add.audio('impact');

        // Spawn enemy cards off the top of the screen.
        this.enemyCardSpawn = {
            x: game.world.centerX,
            y: -200
        };

        // Default time that any messages are displayed for.
        this.messageDisplayTime = 2000;

        // Initialise all the various labels, buttons and graphics that
        // don't move.
        var uiSetup = new UISetup(game, this);
        uiSetup.initialiseUI();

        // Create the input event handlers for the enemy castle
        this.setUpInputForEnemyCastle();

        // The point that the cursor must be above in order to play a 
        // card when it is clicked, otherwise the card will go back to
        // the hand.
        this.pointerPositionToPlayCard = this.castle.top;

        // Setup UI when player starts their turn
        socket.on('startTurn', function (data) {
            playState.displayInfoMessage("Your Turn");
            playState.updateManaLabel(data.mana, data.mana);

            playState.endTurnButton.visible = true;
            playState.enemyTurnLabel.visible = false;
        });

        var cardDrawQueue = [];
        var cardDrawRate = 500;

        // When server sends drawCard event, add the cardData to the cardDrawQueue
        socket.on('drawCard', function (data) {
            // If hand is full, discard the card, otherwise add it to the hand.
            if (data.isHandFull) {
                playState.displaySmallInfoMessage('Your Hand is Full.');
                playState.discardCard(new CardView(game, playState.deck.x, playState.deck.y, 'card-small', data.cardData));
            } else {
                // Add cards onto a queue so that they don't all get drawn at the exact same time.
                cardDrawQueue.push(data.cardData);
            }

            if (data.isDeckEmpty) {
                playState.deck.visible = false;
            }
        });

        // When the server sends the playedCard event, add the card to the board
        // and update the mana label.
        socket.on('playedCard', function (data) {
            playState.addCardToBoard(playState.pickedUpCard);
            playState.pickedUpCard.isPickedUp = false;
            playState.pickedUpCard = 'undefined';
            playState.updateManaLabel(data.currentMana, data.totalMana);

            playState.enableInputForCardsInHand();
            playState.enableInputForCardsOnBoard();
        });

        // When the server lets us know that we successfully carried out an attack.
        socket.on('attacked', function (data) {
            var attacker = playState.selectedCard;
            var target = playState.target;

            playState.deselectCard();
            playState.target = 'undefined';

            if (data.targetHealth <= 0) {
                target.sprite.inputEnabled = false;
            }

            playState.attack(attacker, target, data.attackingCardHealth, data.targetHealth, 100, false);
        });

        // When the server lets us know that the player's health has changed.
        socket.on('updatePlayerHealth', function (playerHealth) {
            playState.castle.setHealth(playerHealth);
        });

        // When the server lets us know that the enemy's health has changed.
        socket.on('updateEnemyHealth', function (enemyHealth) {
            playState.enemyCastle.setHealth(enemyHealth);
        });

        // When the server lets us know that we won.
        socket.on('victory', function () {
            playState.displayInfoMessage("VICTORY!");
            playState.displaySmallInfoMessage("Returning to Main Menu");
            setTimeout(playState.transitionToMenuState, playState.messageDisplayTime);
        });

        // When the server lets us know that we lost.
        socket.on('defeat', function () {
            playState.displayInfoMessage("DEFEAT");
            playState.displaySmallInfoMessage("Returning To Main Menu");
            setTimeout(playState.transitionToMenuState, playState.messageDisplayTime);
        });

        // When the server lets us know that we have no cards left.
        socket.on('outOfCards', function () {
            playState.displaySmallInfoMessage("Out of Cards.");
        });

        // When the server lets us know that it's not our turn
        socket.on('notYourTurn', function () {
            playState.displaySmallInfoMessage("Not Your Turn.");
        });

        // When the server lets us know that we didn't have enough mana to play a card.
        socket.on('notEnoughMana', function () {
            playState.displaySmallInfoMessage("Not Enough Mana.");
        });

        // When the server lets us know that our board is full and we can't play any more cards.
        socket.on('boardIsFull', function () {
            playState.displaySmallInfoMessage("Your Board is Full.");
        });

        // When the server lets us know that a card has already attacked this turn.
        socket.on('cardAlreadyAttacked', function () {
            playState.displaySmallInfoMessage("That card has already attacked this turn.");
        });

        // When the server lets us know that the enemy played a card.
        socket.on('enemyPlayedCard', function (cardData) {
            playState.addCardToEnemyBoard(new CardView(game, playState.enemyCardSpawn.x, playState.enemyCardSpawn.y, 'card-small', cardData));
        });

        // When the server lets us know that the enemy attacked us.
        socket.on('enemyAttacked', function (data) {
            var attacker = playState.enemyBoard.getChildAt(data.attackingCardIndex);
            var target;

            // Use the data we've been sent to determine whether to set the target
            // to be our castle or one of our cards.
            if (data.isTargetCastle) {
                target = playState.castle;
            } else {
                // If the target is dead and was selected by this player, deselect it.
                if (data.targetHealth <= 0) {
                    if (playState.selectedCard == target) {
                        playState.deselectCard();
                    }
                }
                target = playState.board.getChildAt(data.targetIndex);
            }

            playState.attack(attacker, target, data.attackingCardHealth, data.targetHealth, -100, true);
        });

        // When the server lets us know that our enemy disconnected.
        socket.on('enemyDisconnected', function () {
            playState.displayInfoMessage("Opponent Disconnected");
            playState.displaySmallInfoMessage("Returning to Main Menu");
            setTimeout(playState.transitionToMenuState, playState.messageDisplayTime);
        });

        // At regular intervals, create any cards that are on the draw queue and add them to the players hand.
        setInterval(function () {
            if (cardDrawQueue.length > 0) {
                playState.addCardToHand(new CardView(game, playState.deck.x, playState.deck.y, 'card-small', cardDrawQueue[0]));
                cardDrawQueue.splice(cardDrawQueue[0], 1);
            }
        }, cardDrawRate);

        // This allows right-clicking to be used for game input by disabling the default browser behaviour.
        game.canvas.oncontextmenu = function (e) {
            e.preventDefault();
        };

        game.input.onDown.add(function (pointer) {
            // If player left-clicks.      
            if (pointer.button == 0) {
                // If player is holding a card.
                if (playState.pickedUpCard != 'undefined') {
                    // If player clicks within the board area.
                    if (pointer.y < playState.pointerPositionToPlayCard) {
                        socket.emit('playCard', {
                            cardIndex: playState.pickedUpCard.indexInHand
                        });
                    }
                    // If player clicks within the hand area.
                    else {
                        playState.cancelAction();
                    }
                }
            }
            // If player right-clicks, cancel the current action.
            else if (pointer.button == 2) {
                playState.cancelAction();
            }
        });
    },

    cancelAction: function () {
        playState.dropCard();
        playState.deselectCard();
    },

    dropCard: function () {
        if (playState.pickedUpCard != 'undefined') {
            playState.pickedUpCard.isPickedUp = false;

            playState.hand.addChildAt(playState.pickedUpCard, playState.pickedUpCard.indexInHand);

            playState.enableInputForCardsInHand();
            playState.enableInputForCardsOnBoard();

            playState.pickedUpCard = 'undefined';
        }
    },

    deselectCard: function () {
        if (playState.selectedCard != 'undefined') {
            playState.selectedCard.glow.visible = false;
            playState.selectedCard = 'undefined';
        }
    },

    // Notify server that player ended their turn and swap the End Turn button for the Enemy Turn label.
    endTurn: function () {
        socket.emit('endTurn');
        playState.endTurnButton.visible = false;
        playState.enemyTurnLabel.visible = true;
        playState.infoLabel.alpha = 0;
    },

    // Fades to black then starts the menu state.
    transitionToMenuState: function () {
        playState.removeAllSocketListeners();
        game.camera.fade('#000', 1000);
        game.camera.onFadeComplete.addOnce(function () {
            game.state.start('menu', true, false);
        });
    },

    displayInfoMessage: function (message) {
        if (playState.infoLabelTimeout != 'undefined') {
            clearTimeout(playState.infoLabelTimeout);
        }
        playState.infoLabel.text = message;
        playState.infoLabel.alpha = 1;
        playState.infoLabelTimeout = setTimeout(function () {
            playState.infoLabel.alpha = 0;
        }, playState.messageDisplayTime);
    },

    displaySmallInfoMessage: function (message) {
        if (playState.smallInfoLabelTimeout != 'undefined') {
            clearTimeout(playState.smallInfoLabelTimeout);
        }
        playState.smallInfoLabel.text = message;
        playState.smallInfoLabel.alpha = 1;
        playState.smallInfoLabelTimeout = setTimeout(function () {
            playState.smallInfoLabel.alpha = 0;
        }, playState.messageDisplayTime);
    },

    updateManaLabel: function (currentMana, totalMana) {
        playState.manaLabel.setText('Mana: ' + currentMana + '/' + totalMana);
    },

    addCardToHand: function (card) {
        playState.hand.add(card);
        playState.setUpInputForCardInHand(card);
    },

    addCardToBoard: function (card) {
        playState.board.add(card);
        playState.removeInputEventsFromCard(card);
        playState.setUpInputForCardOnBoard(card);
    },

    addCardToEnemyBoard: function (card) {
        playState.enemyBoard.add(card);
        playState.setUpInputForCardOnEnemyBoard(card);
    },

    // Plays animation for discarding a card, then destroys it.
    discardCard: function (card) {
        var discardZone = {
            x: playState.deck.x - 175,
            y: playState.deck.y
        };

        // Animate the cards movement to the discard zone..
        var movementTween = game.add.tween(card).to({
            x: discardZone.x,
            y: discardZone.y
        }, 750, Phaser.Easing.Quadratic.Out);

        // Pause for half a second..
        var waitTween = game.add.tween(card).to({

        }, 500, Phaser.Easing.Linear.None);

        // Then fade the card out..
        var fadeOutTween = game.add.tween(card).to({
            alpha: 0
        }, 750, Phaser.Easing.Linear.None);

        movementTween.chain(waitTween);
        waitTween.chain(fadeOutTween);
        movementTween.start();

        // Then destroy the card.
        fadeOutTween.onComplete.add(function () {
            card.destroy();
        });
    },

    // Plays animation for a card attacking, updates their health
    // labels and then destroys any dead cards.
    attack: function (attacker, target, attackerHealth, targetHealth, strikeOffsetY, isEnemyAttack) {
        // Store the attackers current position so that it can
        // go back to where it came from after the attackTween has finished.
        var startingX = attacker.x;
        var startingY = attacker.y;

        // Dart forward to hit the target.
        var attackTween = game.add.tween(attacker).to({
            x: target.x,
            y: target.y + strikeOffsetY
        }, 50, Phaser.Easing.Linear.None);

        attackTween.onComplete.add(function () {
            attacker.setHealth(attackerHealth);
            target.setHealth(targetHealth);
            playState.impactSound.play();
        });

        // Move back to where the attacker came from.
        var goBackTween = game.add.tween(attacker).to({
            x: startingX,
            y: startingY
        }, 750, Phaser.Easing.Linear.None);

        // Fade the attacker to transparent (used if dead).
        var fadeOutAttackerTween = game.add.tween(attacker).to({
            alpha: 0
        }, 500, Phaser.Easing.Linear.None);

        // Fade the target to transparent (used if dead).
        var fadeOutTargetTween = game.add.tween(target).to({
            alpha: 0
        }, 500, Phaser.Easing.Linear.None);

        // Only start the fade out tweens after the goBackTween has finished
        // and only if the respective card is dead.
        goBackTween.onComplete.add(function () {
            if (attackerHealth <= 0) {
                fadeOutAttackerTween.start();
            }
            if (targetHealth <= 0) {
                fadeOutTargetTween.start();
            }
        });

        // If we are playing a fade out tween then we know the card it is
        // attached to is dead so destroy it after the tween completes.
        fadeOutAttackerTween.onComplete.add(function () {
            if (isEnemyAttack) {
                playState.enemyBoard.remove(attacker);
            } else {
                playState.board.remove(attacker);
            }
            attacker.destroy();
        });
        fadeOutTargetTween.onComplete.add(function () {
            if (isEnemyAttack) {
                playState.board.remove(target);
            } else {
                playState.enemyBoard.remove(target);
            }
            target.destroy();
        });

        // Make the goBackTween start immediately after the attackTween has finished.
        attackTween.chain(goBackTween);
        attackTween.start();
    },

    // Make cards in the hand hoverable and pickupable.
    setUpInputForCardInHand: function (card) {
        card.sprite.inputEnabled = true;
        card.sprite.input.useHandCursor = true;

        card.sprite.events.onInputOver.add(function () {
            card.hover();
        });

        card.sprite.events.onInputOut.add(function () {
            card.unhover();
        });

        // Event for picking up a card from the hand.
        card.sprite.events.onInputDown.add(function () {
            card.unhover();
            card.angle = 0;

            card.isPickedUp = true;
            playState.pickedUpCard = card;

            // Store the the index where the card was held in the hand
            // so that we can put it back there if the player cancels
            // the action.
            card.indexInHand = playState.hand.getChildIndex(card);

            // Temporarily add the card to a new group so that it
            // is drawn in front of all other elements.
            var topGroup = game.add.group();
            topGroup.add(card);
            playState.hand.remove(card);

            // Disable input for the card that has been picked up.
            card.sprite.inputEnabled = false;

            // Disable input for the other cards in the hand
            playState.disableInputForCardsInHand();

            // Disable input for cards on the board
            playState.disableInputForCardsOnBoard();
        });
    },

    // Make cards on the board selectable.
    setUpInputForCardOnBoard: function (card) {
        card.sprite.inputEnabled = true;
        card.sprite.input.useHandCursor = true;

        card.sprite.events.onInputDown.add(function () {
            playState.selectedCard = card;
            card.glow.visible = true;
            playState.board.forEach(function (otherCard) {
                if (otherCard != card) {
                    otherCard.glow.visible = false;
                }
            });
        });
    },

    // Make cards on the enemy board targetable.
    setUpInputForCardOnEnemyBoard: function (card) {
        card.sprite.inputEnabled = true;
        card.sprite.input.useHandCursor = true;
        card.sprite.events.onInputDown.add(function () {
            if (playState.selectedCard != 'undefined') {
                playState.target = card;
                socket.emit('attack', {
                    attackingCardIndex: playState.board.getChildIndex(playState.selectedCard),
                    isTargetCastle: false,
                    targetIndex: playState.enemyBoard.getChildIndex(playState.target)
                });
            } else {
                playState.displaySmallInfoMessage("First select the card you want to attack with.");
            }
        });
    },

    // Make the enemy castle targetable.
    setUpInputForEnemyCastle: function () {
        playState.enemyCastle.sprite.inputEnabled = true;
        playState.enemyCastle.sprite.input.useHandCursor = true;
        playState.enemyCastle.sprite.events.onInputDown.add(function () {
            if (playState.selectedCard != 'undefined') {
                playState.target = playState.enemyCastle;
                socket.emit('attack', {
                    attackingCardIndex: playState.board.getChildIndex(playState.selectedCard),
                    isTargetCastle: true,
                    targetIndex: 'undefined'
                });
            } else {
                playState.displaySmallInfoMessage("First select the card you want to attack with.");
            }
        });
    },

    enableInputForCardsInHand: function () {
        playState.hand.forEach(function (card) {
            card.sprite.inputEnabled = true;
            card.sprite.input.useHandCursor = true;
        });
    },

    enableInputForCardsOnBoard: function () {
        playState.board.forEach(function (card) {
            card.sprite.inputEnabled = true;
            card.sprite.input.useHandCursor = true;
        });
    },

    enableInputForCardsOnEnemyBoard: function () {
        playState.enemyBoard.forEach(function (card) {
            card.sprite.inputEnabled = true;
            card.sprite.input.useHandCursor = true;
        });
    },

    disableInputForCardsInHand: function () {
        playState.hand.forEach(function (card) {
            card.sprite.inputEnabled = false;
        });
    },

    disableInputForCardsOnBoard: function () {
        playState.board.forEach(function (card) {
            card.sprite.inputEnabled = false;
        });
    },

    disableInputForCardsOnEnemyBoard: function () {
        playState.enemyBoard.forEach(function (card) {
            card.sprite.inputEnabled = false;
        });
    },

    removeInputEventsFromCard: function (card) {
        card.sprite.events.onInputDown.removeAll();
        card.sprite.events.onInputOver.removeAll();
        card.sprite.events.onInputOut.removeAll();
    },

    // Removes all the socket event handlers which
    // are used within the playState.
    removeAllSocketListeners: function () {
        socket.removeAllListeners('startTurn');
        socket.removeAllListeners('drawCard');
        socket.removeAllListeners('playedCard');
        socket.removeAllListeners('attacked');
        socket.removeAllListeners('enemyAttacked');
        socket.removeAllListeners('updatePlayerHealth');
        socket.removeAllListeners('updateEnemyHealth');
        socket.removeAllListeners('outOfCards');
        socket.removeAllListeners('notYourTurn');
        socket.removeAllListeners('notEnoughMana');
        socket.removeAllListeners('boardIsFull');
        socket.removeAllListeners('cardAlreadyAttacked');
        socket.removeAllListeners('enemyPlayedCard');
        socket.removeAllListeners('defeat');
        socket.removeAllListeners('victory');
        socket.removeAllListeners('enemyDisconnected');
    }
};