/** HandGroup.js **/

/* This class extends Phaser.Group */

var HandGroup = function (game, x, y) {
    Phaser.Group.call(this, game);
    
    this.handPosition = {
        x: x,
        y: y
    }
};

HandGroup.prototype = Object.create(Phaser.Group.prototype);
HandGroup.prototype.constructor = HandGroup;

/* Override */
HandGroup.prototype.add = function (card) {
    Phaser.Group.prototype.add.call(this, card);

    this.repositionCards();
};

/* Override */
HandGroup.prototype.addChildAt = function (card, index) {
    Phaser.Group.prototype.addChildAt.call(this, card, index);

    this.repositionCards();
};

/* Override */
HandGroup.prototype.remove = function (child) {
    Phaser.Group.prototype.remove.call(this, child);

    this.repositionCards();
};

HandGroup.prototype.repositionCards = function () {

    // Slightly increase the space avaiable for cards in hand as the number of cards increases.
    this.handAreaWidth = 150 + 60 * this.length;
    var maxCardAngle = 5 + 1 * this.length;

    for (var i = 0; i < this.length; i++) {
        var card = this.getChildAt(i);
        var handArea = {
            left: game.world.centerX - this.handAreaWidth / 2,
            right: game.world.centerX + this.handAreaWidth / 2
        }
        var newCardAngle = -maxCardAngle + (1 / (this.length + 1)) * 2 * maxCardAngle * (i + 1);
        var newCardPos = {
            x: handArea.left + (1 / (this.length + 1)) * (handArea.right - handArea.left) * (i + 1),
            y: this.handPosition.y + 1.7 * Math.abs(newCardAngle)
        }

        var tween = game.add.tween(card).to({
            x: newCardPos.x,
            y: newCardPos.y,
            angle: newCardAngle
        }, 500, Phaser.Easing.Quadratic.Out);
        tween.start();

        // Position large versions of cards just above the card in hand
        card.largeCard.position.setTo(newCardPos.x, newCardPos.y - 325);
    }
};