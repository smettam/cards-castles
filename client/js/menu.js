var menuState = {

    playButton: undefined,
    findingMatchLabel: undefined,
    cancelButton: undefined,
    intervalId: undefined,

    create: function () {        
        
        var titleImage = game.add.sprite(0, 0, 'title');

        this.playButton = game.add.button(game.world.centerX, game.world.centerY + 300, 'play', this.findMatch, this, 0, 2, 1, 2);
        this.playButton.anchor.setTo(0.5);
        this.playButton.smoothed = false;

        this.findingMatchLabel = game.add.text(game.world.centerX, game.world.centerY + 300, 'Finding Match', {
            font: '50px Almendra',
            fill: '#fff',
            align: 'center',
            stroke: '#000',
            strokeThickness: 4
        });
        this.findingMatchLabel.anchor.y = 0.5;
        this.findingMatchLabel.x = game.world.centerX - this.findingMatchLabel.width / 2
        this.findingMatchLabel.visible = false;

        this.cancelButton = game.add.button(game.world.centerX, game.world.centerY + 375, 'cancel', this.cancel, this, 2, 0, 1, 0);
        this.cancelButton.anchor.setTo(0.5);
        this.cancelButton.visible = false;
        this.cancelButton.smoothed = false;        
    },

    findMatch: function () {
        socket.emit('play');

        this.playButton.visible = false;
        this.findingMatchLabel.visible = true;
        this.cancelButton.visible = true;

        this.updateFindingMatchLabel();

        socket.on('match found', function () {
            clearInterval(menuState.intervalId);
            menuState.findingMatchLabel.setText('Match Found!');    
            menuState.cancelButton.visible = false;
            setTimeout(menuState.transitionToPlayState, 1000);
        });
    },

    // Fades to black and then starts the play state
    transitionToPlayState: function () {
        game.camera.fade('#000', 1000);
        game.camera.onFadeComplete.add(function () {
            game.state.start('play');
        });
    },

    cancel: function () {
        socket.emit('cancel');

        this.playButton.visible = true;
        this.findingMatchLabel.setText('Finding Match');
        this.findingMatchLabel.visible = false;
        this.cancelButton.visible = false;

        clearInterval(this.intervalId);
    },

    // Indicates to the user that an action is happening by appending a dot
    // to the finding match label each second.
    updateFindingMatchLabel: function () {
        var dots = 1;
        var findingMatchString = 'Finding Match.';
        this.intervalId = setInterval(function () {
            menuState.findingMatchLabel.setText(findingMatchString);

            findingMatchString = findingMatchString.concat('.');
            dots++;

            if (dots == 4) {
                findingMatchString = 'Finding Match';
                dots = 0;
            }
        }, 1000);
    }
}