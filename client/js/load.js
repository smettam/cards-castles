//  The Google WebFont Loader will look for this object, so create it before loading the script.
var WebFontConfig = {    
    
    //  The Google Fonts we want to load (specify as many as you like in the array)
    google: {
        families: ['Almendra', 'Almendra SC']
    }

};

var loadState = {

    preload: function () {

        //  Load the Google WebFont Loader script
        game.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');

        // Add a loading label
        var loadingLabel = game.add.text(game.world.centerX - 0.5, game.world.centerY - 0.5, 'LOADING...', {
            font: '40px Almendra',
            fill: '#fff'
        });
        loadingLabel.anchor.setTo(0.5, 0.5);
        loadingLabel.smoothed = false;

        // Load all assets
        game.load.image('title', 'client/assets/title.png');

        game.load.atlasJSONHash('play', 'client/assets/playbuttonspritesheet.png', 'client/js/playbuttonsprites.json');

        game.load.atlasJSONHash('cancel', 'client/assets/cancelbuttonspritesheet.png', 'client/js/cancelbuttonsprites.json');

        game.load.image('background', 'client/assets/background.png');

        game.load.image('card-small', 'client/assets/card_small.png');

        game.load.image('card-small-glow', 'client/assets/card_small_glow.png');

        game.load.image('card-large', 'client/assets/card_large.png');

        game.load.spritesheet('card-large-glow', 'client/assets/card_large_glow.png', 466, 643);

        game.load.image('particle', 'client/assets/particle.png');

        game.load.image('card-back', 'client/assets/card_back.png');

        game.load.image('castle', 'client/assets/castle.png');

        game.load.atlasJSONHash('endturn', 'client/assets/endturnbuttonspritesheet.png', 'client/js/endturnbuttonsprites.json');

        game.load.image('enemy-turn', 'client/assets/enemy_turn_label.png');

        game.load.image('menu', 'client/assets/menu.png');

        //game.load.audio('music', 'client/assets/audio/menu_music.mp3');

        game.load.audio('battle-music', 'client/assets/audio/battle_music.mp3');

        game.load.audio('impact', 'client/assets/audio/impact.mp3');
    },

    create: function () {

        var music = game.add.audio('battle-music', 1, true);
        music.play();

        game.state.start('menu');
    }
};