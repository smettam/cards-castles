/** CardView.js **/

/* This class extends Phaser.Group */

var CardView = function (game, x, y, image, cardData) {
    Phaser.Group.call(this, game);

    this.position.setTo(x, y);
    this.isPickedUp = false;
    this.indexInHand;

    this.sprite = this.create(0, 0, image);
    this.sprite.anchor.setTo(0.5);
    this.sprite.smoothed = false;   

    if (image == 'card-small') {
        // Create a large card which will be displayed whenever a small card in the hand is hovered over, otherwise it will be invisible.
        this.largeCard = new CardView(game, this.x, this.y, 'card-large',
            cardData);
        this.largeCard.visible = false;

        // Set label positions and styles for small cards relative to the sprite.
        this.costLabelX = this.sprite.left + 22;
        this.costLabelY = this.sprite.top + 18;

        this.attackLabelX = this.sprite.left + 23;
        this.attackLabelY = this.sprite.bottom - 22;

        this.healthLabelX = this.sprite.right - 18;
        this.healthLabelY = this.sprite.bottom - 21;

        this.nameLabelX = this.sprite.x + 3;
        this.nameLabelY = this.sprite.y + 27;

        this.labelStyle = {
            font: '40px Almendra SC',
            fill: '#fff',
            stroke: '#000',
            strokeThickness: 4,
            boundsAlignH: "center"
        };

        this.nameLabelStyle = {
            font: '20px Almendra',
            fill: '#fff',
            stroke: '#000',
            strokeThickness: 3,
            boundsAlignH: "center"
        };

        // Outer green glow which can be made visible to highlight a card.
        this.glow = this.create(0, 0, 'card-small-glow');
        this.glow.anchor.setTo(0.5);
        this.glow.visible = false;

    } else if (image == 'card-large') {
        // Set label positions and styles for large cards relative to the sprite.
        this.costLabelX = this.sprite.left + 51;
        this.costLabelY = this.sprite.top + 38;

        this.attackLabelX = this.sprite.left + 53;
        this.attackLabelY = this.sprite.bottom - 58;

        this.healthLabelX = this.sprite.right - 44;
        this.healthLabelY = this.sprite.bottom - 52;

        this.nameLabelX = this.sprite.x + 7;
        this.nameLabelY = this.sprite.y + 60;

        this.labelStyle = {
            font: '80px Almendra SC',
            fill: '#fff',
            stroke: '#000',
            strokeThickness: 6,
            boundsAlignH: "center"
        };

        this.nameLabelStyle = {
            font: '50px Almendra',
            fill: '#fff',
            stroke: '#000',
            strokeThickness: 6,
            boundsAlignH: "center"
        };

        // Glowing Animation
        this.glow = this.create(10, -10, 'card-large-glow');
        this.glow.anchor.setTo(0.5);
        this.glow.frame = 1;
        this.glow.animations.add('glow', [0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 6, 6, 5, 5, 4, 4, 3, 3, 2, 2, 1, 1], 20, true);
        this.glow.animations.play('glow');
        
        // Particle Emitter
        this.emitter = game.add.emitter(0, 0, 100);
        
        this.addChild(this.emitter);
        this.sendToBack(this.emitter);

        this.emitter.makeParticles('particle');

        this.emitter.minParticleSpeed.setTo(-200, -200);
        this.emitter.maxParticleSpeed.setTo(200, 200);
        
        this.emitter.minParticleScale = 0.4;
        this.emitter.maxParticleScale = 1;
        
        this.emitter.minParticleAlpha = 0.3;
        this.emitter.maxParticleAlpha = 1;
        
        this.emitter.gravity = 0;

        this.emitter.start(false, 2000, 15);
        
        
    }

    // Create the labels and add them to this group.
    this.nameLabel = game.add.text(this.nameLabelX, this.nameLabelY, cardData.name, this.nameLabelStyle, this);
    this.nameLabel.anchor.setTo(0.5);

    this.costLabel = game.add.text(this.costLabelX, this.costLabelY, cardData.cost, this.labelStyle, this);
    this.costLabel.anchor.setTo(0.5);

    this.attackLabel = game.add.text(this.attackLabelX, this.attackLabelY, cardData.attack, this.labelStyle, this);
    this.attackLabel.anchor.setTo(0.5);

    this.healthLabel = game.add.text(this.healthLabelX, this.healthLabelY, cardData.health, this.labelStyle, this);
    this.healthLabel.anchor.setTo(0.5);


};

CardView.prototype = Object.create(Phaser.Group.prototype);
CardView.prototype.constructor = CardView;

CardView.prototype.hover = function () {
    this.largeCard.visible = true;
    this.alpha = 0;
};

CardView.prototype.unhover = function () {
    this.largeCard.visible = false;
    this.alpha = 1;
};

CardView.prototype.setName = function (name) {
    this.nameLabel.setText(name);
};

CardView.prototype.setCost = function (cost) {
    this.costLabel.setText(cost);
};

CardView.prototype.setAttack = function (attack) {
    this.attackLabel.setText(attack);
};

CardView.prototype.setHealth = function (health) {
    this.healthLabel.setText(health);
};

// If the card is picked up then make it follow the mouse pointer.
CardView.prototype.update = function () {
    if (this.isPickedUp) {
        this.position.setTo(game.input.x, game.input.y);
    }
};