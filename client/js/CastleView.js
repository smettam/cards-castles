/** CastleView.js **/

/* This class extends Phaser.Group */

var CastleView = function (game, x, y, image, font) {
    Phaser.Group.call(this, game);

    this.position.setTo(x, y);
    
    this.sprite = this.create(0, 0, image);
    this.sprite.anchor.setTo(0.5);
    this.sprite.smoothed = false;

    this.healthLabel = game.add.text(0, 25, 30, font, this);
    this.healthLabel.anchor.setTo(0.5);
};

CastleView.prototype = Object.create(Phaser.Group.prototype);
CastleView.prototype.constructor = CastleView;

CastleView.prototype.setHealth = function (health) {
    this.healthLabel.setText(health);
};