/** GameController.js **/

/* Dependencies */
Player = require('../model/Player');

/* Constructor */
var GameController = function (players) {
    this.players = players;
    this.currentPlayer;
    this.otherPlayer;
    this.gameOver = false;
};

/* Public Methods */
GameController.prototype.startMatch = function () {
    //Flip coin to determine which player takes the first turn.
    var coinFlip = Math.floor(Math.random() * 2);
    this.currentPlayer = this.players[coinFlip];
    this.otherPlayer = this.players[1 - coinFlip];

    this.currentPlayer.deck.shuffle();
    this.otherPlayer.deck.shuffle();

    // Player going first draws 3 cards
    for (var i = 0; i < 3; i++) {
        this.drawCardForPlayer(this.currentPlayer);
    }

    // Player going second draws an extra card
    for (var i = 0; i < 4; i++) {
        this.drawCardForPlayer(this.otherPlayer);
    }
    this.startTurn();
};

GameController.prototype.startTurn = function () {
    // At the start of their turn, the players total mana is increased and
    // their mana pool is filled.
    this.currentPlayer.incrementTotalMana();
    this.currentPlayer.repelenishMana();

    // Make sure all cards can attack again.
    this.currentPlayer.board.resetCardAttacks();

    // Notify player that their turn has started, and tell
    // them how much mana they now have.
    var mana = this.currentPlayer.totalMana;
    this.currentPlayer.socket.emit('startTurn', {
        mana: mana
    });

    // Draw a card at the start of every turn.
    this.drawCardForPlayer(this.currentPlayer);
};

GameController.prototype.endTurn = function () {
    this.changePlayersTurn();
    this.startTurn();
};

GameController.prototype.changePlayersTurn = function () {
    var temp = this.currentPlayer;
    this.currentPlayer = this.otherPlayer;
    this.otherPlayer = temp;
};

GameController.prototype.playCardAt = function (cardIndex) {

    // Make sure the card exists in the players hand.
    if (typeof this.currentPlayer.hand.getCardAt(cardIndex) != 'undefined') {
        var card = this.currentPlayer.hand.getCardAt(cardIndex);
        // Make sure the player has space on their board to play the card.
        if (!this.currentPlayer.board.isFull()) {
            // Make sure the player has enough mana to play the card.
            if (card.cost <= this.currentPlayer.currentMana) {
                // Remove the card from the players hand and add it to their board
                // and deduct the mana cost.
                this.currentPlayer.playCardAt(cardIndex);

                // Notify the player that they have successfully played the card
                // and send them their new current mana value.
                var newCurrentMana = this.currentPlayer.currentMana;
                var totalMana = this.currentPlayer.totalMana;
                this.currentPlayer.socket.emit('playedCard', {
                    currentMana: newCurrentMana,
                    totalMana: totalMana
                });

                // Notify the other player that their opponent has played a card
                // and send them the card data.
                this.otherPlayer.socket.emit('enemyPlayedCard', {
                    name: card.name,
                    cost: card.cost,
                    attack: card.attack,
                    health: card.health
                });
            } else {
                this.currentPlayer.socket.emit('notEnoughMana');
            }
        } else {
            this.currentPlayer.socket.emit('boardIsFull');
        }
    }
}

GameController.prototype.issueAttack = function (attackingCardIndex, isTargetCastle, targetIndex) {
    // Check that both the attacker and the target exist in each players deck.
    if (typeof this.currentPlayer.board.getCardAt(attackingCardIndex) != 'undefined') {
        var attackingCard = this.currentPlayer.board.getCardAt(attackingCardIndex);

        // Check that the attacker hasn't already attacked this turn.
        if (!attackingCard.hasAttackedThisTurn) {

            if (isTargetCastle) {
                var target = this.otherPlayer;
                attackingCard.attackTarget(target);

                // Notify the attacking player that the attack was successful.
                this.currentPlayer.socket.emit('attacked', {
                    attackingCardHealth: attackingCard.health,
                    targetHealth: target.health
                });

                // Notify the enemy player that their castle has been attacked.
                this.otherPlayer.socket.emit('enemyAttacked', {
                    isTargetCastle: true,
                    attackingCardIndex: attackingCardIndex,
                    attackingCardHealth: attackingCard.health,
                    targetHealth: target.health
                });

                // If the target castle is dead, send Victory/Defeat messages 
                // to the players and end the match.
                if (target.isDead()) {
                    this.currentPlayer.socket.emit('victory');
                    this.otherPlayer.socket.emit('defeat');
                    this.endMatch();
                }
            } else if (typeof this.otherPlayer.board.getCardAt(targetIndex) != 'undefined') {
                var target = this.otherPlayer.board.getCardAt(targetIndex);

                attackingCard.attackTarget(target);

                // Notify the attacking player that the attack was successful.
                this.currentPlayer.socket.emit('attacked', {
                    attackingCardHealth: attackingCard.health,
                    targetHealth: target.health
                });

                // Notify the enemy player that one of their cards has been attacked.
                this.otherPlayer.socket.emit('enemyAttacked', {
                    isTargetCastle: false,
                    attackingCardIndex: attackingCardIndex,
                    targetIndex: targetIndex,
                    attackingCardHealth: attackingCard.health,
                    targetHealth: target.health
                });

            }
        } else {
            this.currentPlayer.socket.emit('cardAlreadyAttacked');
        }
    }

    // Remove any cards that died during the attack.
    this.players.forEach(function (player) {
        player.board.removeDeadCards();
    });
};

GameController.prototype.endMatch = function () {
    this.gameOver = true;
}

GameController.prototype.drawCardForPlayer = function (player) {

    // Determine which player sent the drawCard message so that
    // we know which player to send the enemy action messages to.
    // This is needed in case a player somehow draws a card when it
    // is not their turn. (e.g at the start of the match).
    var otherPlayer;
    if (player == this.currentPlayer) {
        otherPlayer = this.otherPlayer
    } else {
        otherPlayer = this.currentPlayer;
    }

    // If the players deck is not empty, then draw a card
    // and send the card data to the player. Also notify
    // the other player that their opponent drew a card.
    if (!player.deck.isEmpty()) {
        var isHandFull = player.hand.isFull();
        var drawnCard = player.drawCard();
        var isDeckEmpty = player.deck.isEmpty();
        player.socket.emit('drawCard', {
            cardData: drawnCard,
            isHandFull: isHandFull,
            isDeckEmpty: isDeckEmpty
        });
        otherPlayer.socket.emit('enemyDrewCard');
    }
    // If the deck is empty when the player tries to draw a card,
    // the player will take damage based on how many turns their
    // deck has been empty. Notify the player that their deck is
    // empty and send them their new health value. Also notify their
    // opponent.
    else {
        player.turnsOutOfCards++;
        player.takeDamage(player.turnsOutOfCards);
        var playerHealth = player.health;
        player.socket.emit('outOfCards');
        player.socket.emit('updatePlayerHealth', playerHealth)
        otherPlayer.socket.emit('updateEnemyHealth', playerHealth);

        if (player.isDead()) {
            player.socket.emit('defeat');
            otherPlayer.socket.emit('victory');
            this.endMatch();
        }
    }
};



module.exports = GameController;