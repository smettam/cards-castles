/** Board.js **/

/* Constructor */
var Board = function () {
    this.board = [];
};

/* Public Methods */
Board.prototype.addCard = function (card) {
    if (this.isFull()) {
        console.log("Cannot add card to board because it is full.");
        return false;
    }
    this.board.push(card);
    return true;
};

Board.prototype.getCardAt = function (position) {
    return this.board[position];
};

Board.prototype.removeCard = function (card) {
    this.board.splice(this.board.indexOf(card), 1);
};

Board.prototype.removeCardAt = function (position) {
    this.board.splice(position, 1);
};

Board.prototype.isFull = function () {
    return this.board.length >= Board.maxSize;
};

Board.prototype.removeDeadCards = function () {
    for (var i = 0; i < this.board.length; i++) {
        var card = this.getCardAt(i);
        if (card.health <= 0) {
            this.removeCard(card);
        }
    }
};

Board.prototype.resetCardAttacks = function() {
    this.board.forEach(function(card) {
        card.hasAttackedThisTurn = false;
    });
};

/* Static Variables */
Board.maxSize = 6;

module.exports = Board;