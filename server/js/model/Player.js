/** Player.js **/
Board = require('./Board');
Deck = require('./Deck');
Hand = require('./Hand');
Card = require('./Card');

/* Constructor */
var Player = function (socket) {

    /* Instance Variables */
    this.socket = socket;
    this.health = Player.startingHealth;
    this.hand = new Hand();
    this.board = new Board();
    this.deck = new Deck();    
    this.totalMana = 0;
    this.currentMana = 0;
    this.turnsOutOfCards = 0;

    this.populateDeck();
};

/* Public Methods */
Player.prototype.drawCard = function () {
    // Take the top card from the deck and put it into the players hand
    // if there is space, otherwise discard it.
    var drawnCard = this.deck.getTopCard();
    this.hand.addCard(drawnCard);
    this.deck.removeTopCard();
    return drawnCard;
};

Player.prototype.playCardAt = function (cardIndex) {
    var card = this.hand.getCardAt(cardIndex);
    this.board.addCard(card);
    this.hand.removeCard(card);
    this.currentMana -= card.cost;
    return card;
};

Player.prototype.addToDeck = function (card) {
    this.deck.addCard(card);
};

Player.prototype.takeDamage = function (damage) {
    this.health -= damage;
};

Player.prototype.isDead = function () {
    return this.health <= 0;
}

Player.prototype.repelenishMana = function () {
    this.currentMana = this.totalMana;
}

Player.prototype.incrementTotalMana = function () {
    if (this.totalMana < Player.maxMana) {
        this.totalMana++;
    }
}


Player.prototype.populateDeck = function () {
    this.addToDeck(new Card("Peasant"));
    this.addToDeck(new Card("Peasant"));
    this.addToDeck(new Card("Scout"));
    this.addToDeck(new Card("Scout"));
    this.addToDeck(new Card("Footman"));
    this.addToDeck(new Card("Footman"));
    this.addToDeck(new Card("Archer"));
    this.addToDeck(new Card("Archer"));
    this.addToDeck(new Card("Spearman"));
    this.addToDeck(new Card("Spearman"));
    this.addToDeck(new Card("Orc"));
    this.addToDeck(new Card("Orc"));
    this.addToDeck(new Card("Ogre"));
    this.addToDeck(new Card("Ogre"));
    this.addToDeck(new Card("Thief"));
    this.addToDeck(new Card("Thief"));
    this.addToDeck(new Card("Sentry"));
    this.addToDeck(new Card("Sentry"));
    this.addToDeck(new Card("Shield Guard"));
    this.addToDeck(new Card("Shield Guard"));
    this.addToDeck(new Card("Goblin"));
    this.addToDeck(new Card("Goblin"));
    this.addToDeck(new Card("Troll"));
    this.addToDeck(new Card("Troll"));
    this.addToDeck(new Card("Wizard"));
    this.addToDeck(new Card("Wizard"));
    this.addToDeck(new Card("Trebuchet"));
    this.addToDeck(new Card("General"));
    this.addToDeck(new Card("Dragon"));
    this.addToDeck(new Card("Fel Beast"));
};

/* Static Variables */
Player.startingHealth = 30;
Player.maxMana = 10;

module.exports = Player;