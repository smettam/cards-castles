// Import Game Controller
GameController = require('../controllers/GameController');

var Game = function (socket1, socket2) {
    var self = this;

    this.noOfPlayersReady = 0;
    this.players = [new Player(socket1), new Player(socket2)];
    this.gameController = new GameController(this.players);    
};

module.exports = Game;