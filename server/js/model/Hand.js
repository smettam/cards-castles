/** Hand.js **/

/* Constructor */
var Hand = function () {
    this.hand = [];
};

/* Public Methods */
Hand.prototype.clear = function () {
    this.hand = [];
};

Hand.prototype.addCard = function (card) {
    if (this.isFull()) {
        return false;
    }
    this.hand.push(card);
    return true;
};

Hand.prototype.getCardAt = function (position) {
    return this.hand[position];
};

Hand.prototype.removeCard = function (card) {
    this.hand.splice(this.hand.indexOf(card), 1);
};

Hand.prototype.removeCardAt = function (position) {
    this.hand.splice(position, 1);
};

Hand.prototype.getCardCount = function () {
    return this.hand.length;
};

Hand.prototype.isFull = function () {
    return this.hand.length >= Hand.maxSize;
};

/* Static Variables */
Hand.maxSize = 10;

module.exports = Hand;