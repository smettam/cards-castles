/** CardData.js **/

/* Constructor */
var CardData = function (data) {
    this.name = data.name;
    this.attack = data.attack;
    this.health = data.health;
    this.cost = data.cost;
};

/* Static Variables */
CardData.uniqueCards = [];

module.exports = CardData;