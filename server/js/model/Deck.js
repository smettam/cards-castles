/** Deck.js **/

/* Constructor */
var Deck = function () {
    this.deck = [];
};

/* Public Methods */
Deck.prototype.addCard = function (card) {
    this.deck.push(card);
};

Deck.prototype.getCardAt = function (position) {
    return this.deck[position];
};

Deck.prototype.getTopCard = function () {
    return this.deck[this.deck.length - 1];
};

Deck.prototype.removeTopCard = function () {
    this.deck.splice(this.deck.length - 1, 1);
};

/** Knuth Shuffle **/
Deck.prototype.shuffle = function () {
    var currentIndex = this.deck.length;
    var temp;
    var randomIndex;

    while (currentIndex !== 0) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        temp = this.deck[currentIndex];
        this.deck[currentIndex] = this.deck[randomIndex];
        this.deck[randomIndex] = temp;
    }
};

Deck.prototype.isEmpty = function () {
    return this.deck.length == 0;
};

/* Static Variables */
Deck.maxSize = 30;

module.exports = Deck;