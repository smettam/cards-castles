/** Card.js **/

/* Dependencies */
CardData = require('./CardData.js');

/* Constructor */
var Card = function (name) {
    this.name = name;
    this.hasAttackedThisTurn = false;

    for (var i = 0; i < CardData.uniqueCards.length; i++) {
        if (this.name == CardData.uniqueCards[i].name) {
            var matchedCard = CardData.uniqueCards[i];
            this.cost = matchedCard.cost;
            this.attack = matchedCard.attack;
            this.health = matchedCard.health;
        }
    }
};

/* Public Methods */
Card.prototype.takeDamage = function (damage) {
    this.health -= damage;
};

Card.prototype.attackTarget = function (target) {
    target.takeDamage(this.attack);
    if (target instanceof Card) {
        this.takeDamage(target.attack);
    }
    this.hasAttackedThisTurn = true;
};

module.exports = Card;