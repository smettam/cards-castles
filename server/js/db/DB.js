/** DB.js **/
var MongoClient = require('mongodb').MongoClient;

module.exports = {
    findCards: function () {
        return MongoClient.connect('mongodb://localhost:27017/cards').then(function (db) {
            return db.collection('cards').find().toArray();
        }).then(function (items) {
            return items;
        });
    }
};